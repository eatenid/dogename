# 一个用来点名的玩意/Some thing to choose name

![ ](https://github.com/eatenid/dogename/raw/master/top.png)

看下面


## 介绍/Introduce
这是一个以Java语言编写，采用Google Material Design（Google MD）为界面风格的用来点名的东西。
基本上不会有什么大更新，就是改善代码而已了ヾ§ ￣▽)ゞ

由于时间仓促，加上本人还是JAVA的新手，因此代码和算法写的很烂，望谅解ヽ(╯▽╰)ﾉ

#### 用法介绍
待补充

## 截图/Screenshot:

待补充


## 使用到的第三方库/Third-party library used：


[JFoenix](https://github.com/jfoenixadmin/JFoenix)(8.0.4)


[Apache Commons Codec](http://commons.apache.org/proper/commons-codec/)(1.11)

[Gson](https://github.com/google/gson)(2.8.5)

[gushici](https://github.com/xenv/gushici/ "gushici")项目提供的古诗词接口。




